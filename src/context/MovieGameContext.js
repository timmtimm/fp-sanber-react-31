import React, { useState, createContext } from "react";
import axios from 'axios';
import { useHistory } from "react-router-dom";
import { message } from "antd";
import Cookies from "js-cookie";

export const MovieGameContext = createContext();

export const MovieGameProvider = props =>
{
    let history = useHistory();
    const [ dataGame, setDataGame ] = useState([]);
    const [ dataMovie, setDataMovie ] = useState([]);

    const [ detailMovie, setDetailMovie ] = useState([]);
    const [ detailGame, setDetailGame ] = useState([]);

    const [ currentId, setCurrentId ] = useState(null);

    const [ inputGame, setInputGame ] = useState({
        id: "",
        name: "",
        genre: "",
        image_url: "",
        singlePlayer: false, // (true or false) / (1 or 0)
        multiplayer: false, // (true or false) / (1 or 0)
        platform: "",
        release: 2000 // (minimal 2000 dan maksimal 2021)
    });
    const [ inputMovie, setInputMovie ] = useState({
        
        description: "",
        duration: 0,
        genre: "",
        image_url: "",
        rating: 0,  // minimal 0 dan maksimal 10)
        review: "",
        title: "",
        year: 1980
    });

	const fetchDataGame = async () =>
    {
        let result = await axios.get("https://backendexample.sanbersy.com/api/data-game");
        // console.log(result)
        setDataGame(
            result.data.map( (x, index) => {
                return{
                    ...x, number: index + 1
                }
            })
        );
    }

    const fetchDataMovie = async () =>
    {
        let result = await axios.get("https://backendexample.sanbersy.com/api/data-movie");
        setDataMovie(
            result.data.map( (x, index) => {
                return{
                    ...x, number: index + 1
                }}
            )
        );
    }

    const handleDeleteGame = (event) =>
    {
        let idGame = parseInt(event.currentTarget.value);
		// axios.delete(`https://backendexample.sanbersy.com/api/data-game/${idGame}`,)
		// .then((result) => {
			// fetchDataGame();
        //     console.log(result.data);
			// message.success("Delete data succesfully");
		// })
        // .catch((error) => {
            // console.log(error.response.data);
            // message.error("Delete data failed");
        // });
        axios.delete(`https://backendexample.sanbersy.com/api/data-game/${idGame}`,
        {headers: {"Authorization" : "Bearer "+ Cookies.get('token')}})
        .then((result) => {
            // console.log(result.data);
            message.success("Delete data success");
            fetchDataGame();
        })
        .catch((error) => {
            // console.log(error.response.data);
            message.error("Delete data failed");
        });


    }

    const handleDeleteMovie = (event) =>
    {
        // console.log("masuk delete movie")
        let idMovie = parseInt(event.currentTarget.value);
		// axios.delete(`https://backendexample.sanbersy.com/api/data-movie/${idMovie}`)
		// .then((result) => {
		// 	fetchDataGame();
        //     console.log(result.data);
		// 	message.success("Delete data succesfully");
		// })
        // .catch((error) => {
        //     console.log(error.response.data);
        //     message.error("Delete data failed");
        // });

        axios.delete(`https://backendexample.sanbersy.com/api/data-movie/${idMovie}`,
        {headers: {"Authorization" : "Bearer "+ Cookies.get('token')}})
        .then((result) => {
            // console.log(result.data);
            message.success("Delete data success");
            fetchDataGame();
        })
        .catch((error) => {
            // console.log(error.response.data);
            message.error("Delete data failed");
        });
    }

    const handleChangeGame = (event) =>
    {
        let value = event.target.value;
        let name = event.target.name;
        let checkbox = event.target.checked;

        // console.log(`${name}: ${value}`);

        if(name == 'Single Player' || name == 'Multiplayer') setInputGame({...inputGame, [name] : checkbox})
        else setInputGame({...inputGame, [name] : value});
    }

    const handleChangeMovie = (event) =>
    {
        let value = event.target.value;
        let name = event.target.name;

        // console.log(`${name}: ${value}`);
        
        setInputMovie({...inputMovie, [name] : value});
    }

    const getGameByID = async (id) =>
	{
		const result = await axios.get(`https://backendexample.sanbersy.com/api/data-game/${id}`);
        setInputGame(result.data);
	}

    const getMovieByID = async (id) =>
	{
		const result = await axios.get(`https://backendexample.sanbersy.com/api/data-movie/${id}`);
        setInputMovie(result.data);
	}

    const getMovieDetailByID = async (id) =>
    {
        const result = await axios.get(`https://backendexample.sanbersy.com/api/data-movie/${id}`);
        setDetailMovie(result.data);
    }

    const getGameDetailByID = async (id) =>
    {
        const result = await axios.get(`https://backendexample.sanbersy.com/api/data-game/${id}`);
        setDetailGame(result.data);
    }

    const handleSubmitGame = (event) =>
    {
        event.preventDefault();

        // console.log("masuk submit")
        if (currentId === undefined)
		{
            console.log("masuk post")
			axios.post(`http://backendexample.sanbercloud.com/api/mobile-apps`, {
                name: inputGame.name,
                description: inputGame.description,
                category: inputGame.category,
                size: inputGame.size,
                price: inputGame.price,
                rating: inputGame.rating,
                image_url: inputGame.image_url,
                release_year: inputGame.release_year,
                is_android_app: inputGame.is_android_app,
                is_ios_app: inputGame.is_ios_app})
			.then(res => {
				fetchDataGame();
				message.success("Data has been inserted");
			})
		}
		else
		{
            // console.log("masuk put")
			axios.put(`http://backendexample.sanbercloud.com/api/mobile-apps/${currentId}`, {
                name: inputGame.name,
                description: inputGame.description,
                category: inputGame.category,
                size: inputGame.size,
                price: inputGame.price,
                rating: inputGame.rating,
                image_url: inputGame.image_url,
                release_year: inputGame.release_year,
                is_android_app: inputGame.is_android_app,
                is_ios_app: inputGame.is_ios_app})
			.then(res => {
				fetchDataGame();
				message.success("Data has been edited");
			})
		}
        // console.log(inputGame)
        // console.log(currentId)
		setInputGame("");
		setCurrentId(null);
		
		history.push(`/movie/table`);
    }

    return (
        <MovieGameContext.Provider value={{
            inputGame, setInputGame,
            inputMovie, setInputMovie,
            detailMovie, setDetailMovie,
            detailGame, setDetailGame,
            currentId, setCurrentId,
            dataGame, setDataGame,
            dataMovie, setDataMovie,
            fetchDataGame, fetchDataMovie,
            getGameByID, getMovieByID,
            handleDeleteGame, handleDeleteMovie,
            handleChangeGame, handleChangeMovie,
            handleSubmitGame,
            getMovieDetailByID, getGameDetailByID
        }}>
            {props.children}
        </MovieGameContext.Provider>
    );
}