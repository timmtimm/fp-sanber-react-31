import React from 'react';
import { Link } from 'react-router-dom';
import 'antd/dist/antd.css';
import './layout.css';
import { Layout, Menu } from 'antd';
import { UserOutlined, LaptopOutlined, NotificationOutlined } from '@ant-design/icons';
import Cookies from 'js-cookie';
import MenuItem from 'antd/lib/menu/MenuItem';

const { Header, Sider, Content, Footer } = Layout;
const { SubMenu } = Menu;

const LayoutPage = (props) =>
{
    return (
        // <Layout>
        //     <Header style={{ position: 'fixed', zIndex: 1, width: '100%' }}>
        //     <div className="logo" />
            // <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['1']} >
            //     <Menu.Item key={'1'}><Link to="/">Home</Link></Menu.Item>
            //     <Menu.Item key={'2'}><Link to="/movie-table">Movie Table</Link></Menu.Item>
            //     <Menu.Item key={'3'}><Link to="/game-table">Game Table</Link></Menu.Item>
            //         <Menu.Item key={'4'}><Link to="/login">Login</Link></Menu.Item>
            //         <Menu.Item key={'5'}><Link to="/register">Sign Up</Link></Menu.Item>
            // </Menu>
        //     </Header>
        //     <Sider width={200} className="site-layout-background">
        //         <Menu
        //         mode="inline"
        //         defaultSelectedKeys={['1']}
        //         defaultOpenKeys={['sub1']}
        //         style={{ height: '100%', borderRight: 0 }}
        //         >
        //         <SubMenu key="sub1" icon={<UserOutlined />} title="subnav 1">
        //             <Menu.Item key="1">option1</Menu.Item>
        //             <Menu.Item key="2">option2</Menu.Item>
        //             <Menu.Item key="3">option3</Menu.Item>
        //             <Menu.Item key="4">option4</Menu.Item>
        //         </SubMenu>
        //         <SubMenu key="sub2" icon={<LaptopOutlined />} title="subnav 2">
        //             <Menu.Item key="5">option5</Menu.Item>
        //             <Menu.Item key="6">option6</Menu.Item>
        //             <Menu.Item key="7">option7</Menu.Item>
        //             <Menu.Item key="8">option8</Menu.Item>
        //         </SubMenu>
        //         <SubMenu key="sub3" icon={<NotificationOutlined />} title="subnav 3">
        //             <Menu.Item key="9">option9</Menu.Item>
        //             <Menu.Item key="10">option10</Menu.Item>
        //             <Menu.Item key="11">option11</Menu.Item>
        //             <Menu.Item key="12">option12</Menu.Item>
        //         </SubMenu>
        //         </Menu>
        //     </Sider>
        //     <Content className="site-layout" style={{ padding: '50px 50px 0px 50px', marginTop: 64 }}>
        //         <div className="site-layout-background" style={{ padding: 24, minHeight: 380 }}>
        //             {props.content}
        //         </div>
        //     </Content>
            // <Footer style={{ textAlign: 'center' }}>Submission for Sanber React JS<br />Created By Timotius Wirawan</Footer>
        // </Layout>
        <>
            <Layout>
                <Header className="header">
                <div className="logo" />
                <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['1']}  style={{display:"flex"}}>
                    <Menu.Item key={'1'}><Link to="/">Home</Link></Menu.Item>
                    <Menu.Item key={'2'}><Link to="/movie/table">Movie Table</Link></Menu.Item>
                    <Menu.Item key={'3'}><Link to="/game/table">Game Table</Link></Menu.Item>
                    <Menu.Item key={'4'} style={{marginLeft: "auto", display: Cookies.get('token') == undefined ? "block" : "none"}}><Link to="/login">Login</Link></Menu.Item>
                    <Menu.Item key={'5'} style={{display: Cookies.get('token') == undefined ? "block" : "none"}}><Link to="/register">Sign Up</Link></Menu.Item>
                </Menu>
                </Header>

                <Layout>
                    <Sider style={{display: Cookies.get('token') != undefined ? "block" : "none"}} width={200} className="site-layout-background">
                        <Menu
                            mode="inline"
                            defaultSelectedKeys={['1']}
                            defaultOpenKeys={['sub1']}
                            style={{ height: '100%', borderRight: 0 }}
                        >
                            <SubMenu key="sub1" icon={<UserOutlined />} title={ Cookies.get('token') && Cookies.get('user') != undefined ? JSON.parse(Cookies.get('user')).name : "" }>
                                <Menu.Item key="1"><Link to="/change-password">Change Password</Link></Menu.Item>
                                <Menu.Item key="2" onClick={() => { Cookies.remove('token'); window.location = "/"; }}>Log out</Menu.Item>
                            </SubMenu>
                            <SubMenu key="sub2" icon={<LaptopOutlined />} title="Add Title">
                                <Menu.Item key="5"><Link to="/movie/form">Add Movie</Link></Menu.Item>
                                <Menu.Item key="6"><Link to="/game/form">Add Game</Link></Menu.Item>
                            </SubMenu>
                        </Menu>
                    </Sider>

                    <Content
                        className="site-layout-background"
                        style={{ padding: 24, margin: 0, minHeight: 280 }}
                    >
                        {props.content}
                    </Content>
                </Layout>

                <Footer style={{ textAlign: 'center' }}>Submission for Sanber React JS<br />Created By Timotius Wirawan</Footer>
            </Layout>
        </>
    );
}

export default LayoutPage;