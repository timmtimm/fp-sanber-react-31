import React, { useContext, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import 'antd/dist/antd.css';
import TextArea from "antd/lib/input/TextArea";
import Checkbox from "antd/lib/checkbox/Checkbox";
import { Form, Input, Button, Select } from 'antd';
import { MovieGameContext } from "../context/MovieGameContext";
import axios from "axios";
import Cookies from "js-cookie";
import { message } from "antd";
import { useHistory } from "react-router-dom";


const MovieForm = () =>
{
    const { inputMovie, setCurrentId, getMovieByID } = useContext(MovieGameContext);

    const [form] = Form.useForm();

    let { Id } = useParams();

    let history = useHistory();

    const [ fetched, setFetched ] = useState(false);

    useEffect(() => {
        if(fetched == false)
        {
            if(Id !== undefined)
            {
                // console.log("masuk");
                getMovieByID(Id);
            }
            setFetched(true);
            setCurrentId(Id);
        }
        // console.log(inputMovie);
        // console.log(Id);
        form.setFieldsValue({...inputMovie});
    }, [inputMovie]);

    const onFinish = (values) =>
    {
        // console.log("Finish:", values)

        if(Id != undefined)
        {
            axios.put(`https://backendexample.sanbersy.com/api/data-movie/${Id}`,
                { ...values }, {headers: {"Authorization" : "Bearer "+ Cookies.get('token')}}
            ).then((result) => {
                // console.log(result.data);
                message.success('Data has been edited');
                history.push('/movie/table');
            }).catch((error) => {
                // console.log(error.response.data);
                message.error('Data failed edited');
            });
        }
        else
        {
            axios.post(`https://backendexample.sanbersy.com/api/data-movie`,
                { ...values }, {headers: {"Authorization" : "Bearer "+ Cookies.get('token')}}
            ).then((result) => {
                message.success('Data added successfully');
                history.push('/movie/table');
            }).catch((error) => {
                message.error('Data failed added');
            });
        }
    }

    return(
        <>
            <h1 style={{textAlign: "center", fontSize: "2.5rem"}}>Form Movie</h1>
            <div style={{width:"100%"}}>
                <Form
                    name="basic" form={form}
                    onFinish={onFinish}
                    labelCol={{ span: 4 }}
                    wrapperCol={{ span: 20 }}
                    initialValues={{ remember: true }}
                    autoComplete="off"
                    style={{width:"90%"}}
                >
                    <Form.Item
                        name="title"
                        label="Title"
                        rules={[{ required: true, message: `Please insert Title` }]}
                    >
                        <Input type="text" placeholder="" name="title" value="" />
                    </Form.Item>

                    <Form.Item
                        name="genre"
                        label="Genre"
                        rules={[{ required: true, message: `please insert Genre` }]}
                    >
                        <Input type="text" placeholder="" name="genre" value=""/>
                    </Form.Item>

                    <Form.Item
                        name="description"
                        label="Description"
                        rules={[{ required: true, message: `Please insertDescription` }]}
                    >
                        <TextArea placeholder="" name="description" value=""/>
                    </Form.Item>
                    
                    <Form.Item
                        name="year"
                        label="Release Year"
                        rules={[{ required: true, message: `Please insert Release Year` }]}
                    >
                        <Input type="number" min={1980} max={2021} placeholder="" name="year" value=""/>
                    </Form.Item>
                    
                    <Form.Item
                    name="duration" 
                    label="Duration in minute"
                    rules={[{ required: true, message: `Please insert Movie Duration` }]}
                    >
                        <Input type="number" placeholder="" name="duration" value=""/>
                    </Form.Item>
                    <Form.Item
                        name="rating"
                        label="Rating"
                        rules={[{ required: true, message: `Please insert Rating` }]}
                    >
                        <Input type="number" min={0} max={10} placeholder="" name="rating" value="0"/>
                    </Form.Item>

                    <Form.Item 
                        name="image_url"
                        label="Image Url"
                        rules={[{ required: true, message: `Please insert Image` }]}
                    >
                        <Input type="text" placeholder="" name="image_url" value=""/>
                    </Form.Item>

                    <Form.Item
                        name="review"
                        label="Review"
                        rules={[{ required: true, message: `Please insert Review` }]}
                    >
                        <TextArea placeholder="" name="review" value=""/>
                    </Form.Item>

                    <Form.Item wrapperCol={{ offset: 4, span: 20 }}>
                        <Button type="primary" htmlType="submit"> Submit </Button>
                    </Form.Item>
                </Form>
            </div>
        </>
    )
}

export default MovieForm;