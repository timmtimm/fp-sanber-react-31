import React, { useContext, useEffect, useState } from "react";
import { useHistory, useParams } from "react-router-dom";
import { MovieGameContext } from "../context/MovieGameContext";

const GameDetail = () =>
{
    let history = useHistory();

    const { detailGame, getGameDetailByID } = useContext(MovieGameContext) ;

    const [ fetched, setFetched ] = useState(false);

    const { Id } = useParams();

    useEffect(() => {
        if(fetched == false)
        {
            if(Id !== undefined)
            {
                // console.log("masukx");
                // console.log(Id)
                getGameDetailByID(Id);
                // console.log(inputGame)
                // console.log(dataGame)
            }
            setFetched(true);
        }
        // console.log(detailGame);
        // console.log(Id);
        // form.setFieldsValue({...inputGame});
    }, [getGameDetailByID]);

    // id: "",
    // name: "",
    // genre: "",
    // image_url: "",
    // singlePlayer: false, // (true or false) / (1 or 0)
    // multiplayer: false, // (true or false) / (1 or 0)
    // platform: "",
    // release: 2000 // (minimal 2000 dan maksimal 2021)

    return(
        <div>
            <img src={detailGame.image_url} width={500} style={{display: "block", margin: "auto"}}/>
            <h1 style={{fontSize: "2rem", textAlign: "center"}}>{detailGame.name} ({detailGame.release})</h1>
            <hr />
            {/* <p><strong style={{fontSize: "1.25rem"}}>Releae year</strong><br /><span style={{fontSize: "1rem"}}></span></p> */}
            <p><strong style={{fontSize: "1.25rem"}}>Genre</strong><br /><span style={{fontSize: "1rem"}}>{detailGame.genre}</span></p>
            <p><strong style={{fontSize: "1.25rem"}}>Platform</strong><br /><span style={{fontSize: "1rem"}}>{detailGame.platform} Minute</span></p>
            <p><strong style={{fontSize: "1.25rem"}}>Mode</strong><br /><span style={{fontSize: "1rem"}}>{detailGame.singlePlayer == true ? "Single Player" : ""} { detailGame.singlePlayer && detailGame.multiplayer == true ? "& " : "" }{detailGame.multiplayer == true ? "Multiplayer" : ""}</span></p>
        </div>
    );
}

export default GameDetail;