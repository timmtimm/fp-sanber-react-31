import React, { useState, useEffect } from 'react';
import "antd/dist/antd.css";
import { Form, Input, Button, message } from 'antd';
import { UserOutlined, LockOutlined, MailOutlined } from '@ant-design/icons';
import axios from 'axios';
import Cookies from "js-cookie"

const RegisterForm = () =>
{
    
    const [form] = Form.useForm();
    const [, forceUpdate] = useState({}); // To disable submit button at the beginning.
    
    useEffect(() => {
        forceUpdate({});
    }, []);
    
    const onFinish = (values) => {
        // console.log('Finish:', values);

        axios.post("https://backendexample.sanbersy.com/api/register", {
            name: values.name,
            email: values.email,
            password: values.password
        })
        .then((result) => {
            // console.log(result.data);
            Cookies.set("token", result.data.token, {expires: 1});
            Cookies.set("user", JSON.stringify(result.data.user), {expires: 1});

            message.success("You has been registered");

            window.location = "/";
        })
        .catch((error) => {
            // console.log(error.response.data)
            message.error("Email has been used");
        })
    };
    
    return(
        <>
            <h1 style={{textAlign: "center", fontSize: "2.5rem"}}>Sign Up</h1>
            <Form form={form} name="horizontal_login" style={{justifyContent: "center"}} layout="inline" onFinish={onFinish}>
                <Form.Item
                    name="name"
                    rules={[
                    {
                        required: true,
                        message: 'Please input your name!',
                    },
                    ]}
                >
                    <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Name" />
                </Form.Item>
                <Form.Item
                    name="email"
                    rules={[
                        {
                            required: true,
                            message: 'Please input your email!',
                        },
                    ]}
                >
                    <Input
                    prefix={<MailOutlined className="site-form-item-icon" />}
                    type="email"
                    placeholder="email"
                    />
                </Form.Item>
                <Form.Item
                    name="password"
                    rules={[
                        {
                            required: true,
                            message: 'Please input your password!',
                        },
                    ]}
                >
                    <Input
                    prefix={<LockOutlined className="site-form-item-icon" />}
                    type="password"
                    placeholder="Password"
                    />
                </Form.Item>
                <Form.Item shouldUpdate>
                    {() => (
                    <Button
                        type="primary"
                        htmlType="submit"
                        disabled={
                        !form.isFieldsTouched(true) ||
                        !!form.getFieldsError().filter(({ errors }) => errors.length).length
                        }
                    >
                        Submit
                    </Button>
                    )}
                </Form.Item>
            </Form>
		</>
    );
}

export default RegisterForm;