import React, { useContext, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import 'antd/dist/antd.css';
import TextArea from "antd/lib/input/TextArea";
import Checkbox from "antd/lib/checkbox/Checkbox";
import { Form, Input, Button, Select } from 'antd';
import { MovieGameContext } from "../context/MovieGameContext";
import axios from "axios";
import Cookies from "js-cookie";
import { message } from "antd";
import { useHistory } from "react-router-dom";

const GameForm = () =>
{
    const { inputGame, setCurrentId, getGameByID } = useContext(MovieGameContext);

    const [form] = Form.useForm();

    let { Id } = useParams();

    let history = useHistory();

    const [ fetched, setFetched ] = useState(false);

    useEffect(() => {
        if(fetched == false)
        {
            if(Id !== undefined)
            {
                // console.log("masuk");
                getGameByID(Id);
            }
            setFetched(true);
            setCurrentId(Id);
        }
        // console.log(inputGame);
        // console.log(Id);
        form.setFieldsValue({...inputGame});
    }, [inputGame]);
    
    const onFinish = (values) =>
    {
        // console.log("Finish:", values);

        if(Id != undefined)
        {
            axios.put(`https://backendexample.sanbersy.com/api/data-game/${Id}`,
                { ...values }, {headers: {"Authorization" : "Bearer "+ Cookies.get('token')}}
            ).then((result) => {
                // console.log(result.data);
                message.success('Data has been edited');
                history.push('/game/table');
            }).catch((error) => {
                // console.log(error.response.data);
                message.error('Data failed edited');
            });
        }
        else
        {
            axios.post(`https://backendexample.sanbersy.com/api/data-game`,
                { ...values }, {headers: {"Authorization" : "Bearer "+ Cookies.get('token')}}
            ).then((result) => {
                message.success('Data added successfully');
                history.push('/game/table');
            }).catch((error) => {
                message.error('Data failed added');
            });
        }
    }

    return(
        <>
            <h1 style={{textAlign: "center", fontSize: "2.5rem"}}>Form Game</h1>
            <div style={{width:"100%"}}>
                <Form 
                    name="basic" form={form}
                    onFinish={onFinish}
                    labelCol={{ span: 4 }}
                    style={{width:"90%"}}
                >
                    <Form.Item
                        name="name"
                        label="Name"
                        rules={[{ required: true, message: `Please insert Name` }]}
                    >
                        <Input type="text" placeholder="" name="name" value="" />
                    </Form.Item>

                    <Form.Item
                        name="genre"
                        label="Genre"
                        rules={[{ required: true, message: `Please insert Genre` }]}
                    >
                        <Input type="text" placeholder="" name="genre" value=""/>
                    </Form.Item>

                    <Form.Item
                    name="release"
                    label="Release Year"
                    rules={[{ required: true, message: `Please insert Release Year` }]}
                    >
                        <Input type="number" min={1980} max={2021} placeholder="" name="release" value="1980"/>
                    </Form.Item>

                    <Form.Item
                        name="image_url"
                        label="Image Url"
                        rules={[{ required: true, message: `Please insert Image` }]}
                    >
                        <Input type="text" placeholder="" name="image_url" value=""/>
                    </Form.Item>

                    <Form.Item
                        name="platform"
                        label="Platform"
                        rules={[{ required: true, message: `Please insert Platform` }]}
                    >
                        <TextArea placeholder="" name="platform" value=""/>
                    </Form.Item>

                    <Form.Item
                        name="singlePlayer"
                        label="Single Player"
                        valuePropName="checked"
                    >
                        <Checkbox></Checkbox>
                    </Form.Item>

                    <Form.Item
                        name="multiplayer"
                        label="Multi Player"
                        valuePropName="checked"
                        wrapperCol={{ offset: 0, span: 24 }}
                    >
                        <Checkbox></Checkbox>
                    </Form.Item>

                    <Form.Item wrapperCol={{ offset: 4, span: 20 }}>
                        <Button type="primary" htmlType="submit"> Submit </Button>
                    </Form.Item>
                </Form>
            </div>
        </>
    )
}

export default GameForm;