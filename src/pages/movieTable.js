import React, { useContext, useEffect } from "react";
import { MovieGameContext, MovieGameProvider } from "../context/MovieGameContext";
import { Table, Button, message } from "antd";
import { Link } from "react-router-dom";
import { DeleteFilled, EditFilled } from '@ant-design/icons';
import Cookies from "js-cookie";

const MobileList = () =>
{
    const { dataMovie, fetchDataMovie, handleDeleteMovie } = useContext(MovieGameContext);

    useEffect(() => {
        fetchDataMovie();
    }, []);

    const columns = [
        {title:'No', dataIndex: 'number', key: 'number'},
        {title:'Title', dataIndex: 'title', key: 'title'},
        {title:'Genre', dataIndex: 'genre', key: 'genre'},
        {title:'Year', dataIndex: 'year', key: 'year'},
        {title:'Rating', dataIndex: 'rating', key: 'rating'},
        {title:'Review', dataIndex: 'review', key: 'review'},
        {title:'Description', dataIndex: 'description', key: 'description'},
        {title:'Action', key: 'action', render: (data, index) => (
            <>
            {
                Cookies.get('token') == undefined
                ?
                <Button icon={<EditFilled /> } onClick={() => { message.error("Cannot Edit Data. Login First!")}} style={{margin:"0.25rem"}}>Edit</Button>
                :
                <Link to={`/movie/form/edit/${data.id}`}>
                    <Button icon={<EditFilled /> } style={{margin:"0.25rem"}}>Edit</Button>
                </Link>
            }
            {
                Cookies.get('token') == undefined
                ?
                <Button icon={<DeleteFilled />} danger onClick={() => { message.error("Cannot Delete Data. Login First!") }} value={data.id} style={{margin:"0.25rem"}}>Delete</Button>
                :
                <Button icon={<DeleteFilled />} danger onClick={handleDeleteMovie} value={data.id} style={{margin:"0.25rem"}}>Delete</Button>
            }
            </>
        )},
    ];

    return(
        <MovieGameProvider>
        <h1 style={{textAlign: "center", fontSize: "2.5rem"}}>Movie List</h1>
        <div style={{ textAlign: "center", marginBottom: "20px" }}>
        </div>
        <Table columns={columns} dataSource={dataMovie} id="appTable" rowKey={record => record.id} pagination={{ position: ["bottomCenter"] }} />
        </MovieGameProvider>
    );
}

export default MobileList;