import React, { useContext, useEffect } from "react";
import { MovieGameContext, MovieGameProvider } from "../context/MovieGameContext";
import { Card, Col, Row } from 'antd';
import { Link, useHistory } from "react-router-dom";

const { Meta } = Card;

const Home = () =>
{
    const { dataGame, fetchDataGame, dataMovie, fetchDataMovie } = useContext(MovieGameContext);

    let history = useHistory();

    useEffect(() => {
        fetchDataMovie();
        fetchDataGame();
    }, []);


    return(
        <MovieGameProvider>
            <h1 style={{textAlign: "center", fontSize: "2.5rem"}}>Movie List</h1>
            <div className="site-card-wrapper">
                <Row gutter={16}>
            {
                dataMovie.map((x) => {
                    return (
                        <>
                            {/* <Link to={`/movie/detail/${event.currentTarget.Id}`}> */}
                                <Col span={6} onClick={() => { history.push(`/movie/detail/${x.id}`) }} style={{marginTop: "1rem"}}>
                                    <Card
                                    hoverable
                                    cover={<img alt={`Image ${x.title}`} style={{objectFit: "cover", height: "50vh"}} src={x.image_url}
                                    />}
                                >
                                    <Meta title={`${x.title} (${x.year})`} description={` `} />
                                    <div style={{color: "rgba(0, 0, 0, 0.45)"}}>Genre: {x.genre}</div>
                                    <div style={{color: "rgba(0, 0, 0, 0.45)"}}>Rating: {x.rating}</div>
                                    <div style={{color: "rgba(0, 0, 0, 0.45)"}}>Review: {x.review}</div>
                                    <div style={{color: "rgba(0, 0, 0, 0.45)"}}>Description: {x.description}</div>
                                </Card>
                                </Col>
                            {/* </Link> */}
                        </>
                    )
                })
            }
                </Row>
            </div>

            <h1 style={{textAlign: "center", fontSize: "2.5rem", marginTop: "3rem"}}>Game List</h1>

            <div className="site-card-wrapper">
                <Row gutter={16}>
            {
                dataGame.map((x) => {
                    return (
                        <Col span={6} onClick={() => { history.push(`/game/detail/${x.id}`) }} style={{marginTop: "1rem"}}>
                            <Card
                                hoverable
                                cover={<img alt={`Image ${x.name}`} style={{objectFit: "cover", height: "50vh"}} src={x.image_url} />}
                            >
                                <Meta title={`${x.name} (${x.release})`} description={` `} />
                                <div style={{color: "rgba(0, 0, 0, 0.45)"}}>Genre: {x.genre}</div>
                                <div style={{color: "rgba(0, 0, 0, 0.45)"}}>Platform: {x.platform}</div>
                                <div style={{color: "rgba(0, 0, 0, 0.45)"}}>Mode: {x.singlePlayer == true ? "Single Player" : ""} { x.singlePlayer && x.multiplayer == true ? "& " : "" }{x.multiplayer == true ? "Multiplayer" : ""}</div>
                            </Card>
                        </Col>
                    )
                })
            }
                </Row>
            </div>
            
        </MovieGameProvider>
    )
}

export default Home;