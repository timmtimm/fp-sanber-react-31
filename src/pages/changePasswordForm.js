import React, { useContext, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import 'antd/dist/antd.css';
import TextArea from "antd/lib/input/TextArea";
import Checkbox from "antd/lib/checkbox/Checkbox";
import { Form, Input, Button, Select } from 'antd';
import { MovieGameContext } from "../context/MovieGameContext";
import axios from "axios";
import Cookies from "js-cookie";
import { message } from "antd";
import { useHistory } from "react-router-dom";

const ChangePasswordForm = () =>
{
    const { inputGame, setCurrentId, getGameByID } = useContext(MovieGameContext);

    const [form] = Form.useForm();

    let { Id } = useParams();

    let history = useHistory();

    const [ fetched, setFetched ] = useState(false);

    useEffect(() => {
        // if(fetched == false)
        // {
        //     if(Id !== undefined)
        //     {
        //         // console.log("masuk");
        //         getGameByID(Id);
        //     }
        //     setFetched(true);
        //     setCurrentId(Id);
        // }
        // // console.log(inputGame);
        // // console.log(Id);
        // form.setFieldsValue({...inputGame});
    }, []);
    
    const onFinish = (values) =>
    {
        // console.log("Finish:", values);
        axios.post(`https://backendexample.sanbersy.com/api/change-password`,
        { ...values }, {headers: {"Authorization" : "Bearer "+ Cookies.get('token')}}
        )
        .then((result) => {
            message.success('Password changed successfully');
        })
        .catch((error) => {
            let response = JSON.parse(error.response.data);

            if(response.current_password) message.error(response.current_password);
            if(response.new_confirm_password) message.error(response.new_confirm_password);
        });
    }

    return(
        <>
            <h1 style={{textAlign: "center", fontSize: "2.5rem"}}>Change Password</h1>
            <div style={{width:"100%"}}>
                <Form  name="basic" form={form}
                        onFinish={onFinish}
                        labelCol={{ span: 7 }}
                        wrapperCol={{ span: 10 }}
                        initialValues={{ remember: true }}
                        autoComplete="off" style={{width:"75%", padding:"0px", margin:"0px auto"}}>
                    <Form.Item
                        name="current_password"
                        label="Current password"
                        rules={[{ required: true, message: `Current password is required!` },{min:6, message:"Current password must be at least 6 characters"}]}
                    >
                        <Input type="password" placeholder="" name="current_password" value="" />
                    </Form.Item>

                    <Form.Item
                        name="new_password"
                        label="New Password"
                        rules={[{ required: true, message: `New Password is required!` }, {min:6, message:"New Password must be at least 6 characters"}]}
                    >
                        <Input type="password" placeholder="" name="new_password" value="" />
                    </Form.Item>

                    <Form.Item
                        name="new_confirm_password"
                        label="New Confirm Password"
                        rules={[{ required: true, message: `New Confirm Password is required!` }, {min:6, message:"New Confirm Password must be at least 6 characters"}]}
                    >
                        <Input type="password" placeholder="" name="new_confirm_password" value="" />
                    </Form.Item>

                    <Form.Item wrapperCol={{ offset: 7, span: 20 }}>
                        <Button type="primary" htmlType="submit"> Submit </Button>
                    </Form.Item>
                </Form>

            </div>
        </>
    )
}

export default ChangePasswordForm;