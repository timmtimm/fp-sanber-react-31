import React, { useState, useEffect } from 'react';
import "antd/dist/antd.css";
import { Form, Input, Button, message } from 'antd';
import { MailOutlined, LockOutlined } from '@ant-design/icons';
import axios from 'axios';
import Cookies from 'js-cookie';

const LoginForm = () =>
{
    
    const [form] = Form.useForm();
    const [, forceUpdate] = useState({}); // To disable submit button at the beginning.
    
    useEffect(() => {
        forceUpdate({});
    }, []);
    
    const onFinish = (values) => {
        // console.log('Finish:', values);

        axios.post("https://backendexample.sanbersy.com/api/user-login", {
            email: values.email,
            password: values.password
        })
        .then((result) => {
            // console.log(result.data);
            Cookies.set("token", result.data.token, {expires:1});
            Cookies.set("user", JSON.stringify(result.data.user), {expires:1});
            message.success(`Welcome ${result.data.user.name}`);
            window.location = "/";
        })
        .catch((error) => {
            // console.log(error.response.data);
            message.error("Wrong Email or Password");
        })
    };
    
    return(
        <>
            <h1 style={{textAlign: "center", fontSize: "2.5rem"}}>Login</h1>
            <Form form={form} name="horizontal_login" style={{justifyContent: "center"}} layout="inline" onFinish={onFinish}>
            <Form.Item
                    name="email"
                    rules={[
                        {
                            required: true,
                            message: 'Please input your email!',
                        },
                    ]}
                >
                    <Input
                    prefix={<MailOutlined className="site-form-item-icon" />}
                    type="email"
                    placeholder="email"
                    />
                </Form.Item>
                <Form.Item
                    name="password"
                    rules={[
                    {
                        required: true,
                        message: 'Please input your password!',
                    },
                    ]}
                >
                    <Input
                    prefix={<LockOutlined className="site-form-item-icon" />}
                    type="password"
                    placeholder="Password"
                    />
                </Form.Item>
                <Form.Item shouldUpdate>
                    {() => (
                    <Button
                        type="primary"
                        htmlType="submit"
                        disabled={
                        !form.isFieldsTouched(true) ||
                        !!form.getFieldsError().filter(({ errors }) => errors.length).length
                        }
                    >
                        Submit
                    </Button>
                    )}
                </Form.Item>
            </Form>
		</>
    );
}

export default LoginForm;