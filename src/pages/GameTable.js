import React, { useContext, useEffect } from "react";
import { MovieGameContext, MovieGameProvider } from "../context/MovieGameContext";
import { Table, Button, message } from "antd";
import { Link } from "react-router-dom";
import { DeleteFilled, EditFilled } from '@ant-design/icons';
import Cookies from "js-cookie";

const GameTable = () =>
{
    const { dataGame, fetchDataGame, handleDeleteGame } = useContext(MovieGameContext);

    useEffect(() => {
        fetchDataGame();
    }, []); 

    // name: x.name,
    // genre: x.genre,
    // image_url: x.image_url,
    // singlePlayer: x.singlePlayer, // (true or false) / (1 or 0)
    // multiplayer: x.multiplayer, // (true or false) / (1 or 0)
    // platform: x.platform,
    // release: x.release // (minimal 2000 dan maksimal 2021)

    const columns = [
        {title:'No', dataIndex: 'number', key: 'number'},
        {title:'Name', dataIndex: 'name', key: 'name'},
        {title:'Genre', dataIndex: 'genre', key: 'genre'},
        {title:'Mode', render: (record) => record.singlePlayer ? (record.multiplayer ? `Single Player, Multiplayer` : 'Single Player') : (record.multiplayer ? 'Multiplayer' : ''), key: 'Mode'},
        {title:'Platform', dataIndex: 'platform', key: 'platform'},
        {title:'Release Year', dataIndex: 'release', key: 'release'},
        {title:'Action', key: 'action', render: (data, index) => (
            <div style={{textAlign: "center"}}>
                {
                    Cookies.get('token') == undefined
                    ?
                    <Button icon={<EditFilled />} onClick={() => { message.error("Cannot Edit Data. Login First!") }} style={{margin:"0.25rem"}}>Edit</Button>
                    :
                    <Link to={`/game/form/edit/${data.id}`}>
                        <Button icon={<EditFilled />} style={{margin:"0.25rem"}}>Edit</Button>
                    </Link>
                    
                }
                {
                    Cookies.get('token') == undefined
                    ?
                    <Button icon={<DeleteFilled />} danger onClick={() => { message.error("Cannot Delete Data. Login First!") }} style={{margin:"0.25rem"}} value={data.id}>Delete</Button>
                    :
                    <Button icon={<DeleteFilled />} danger onClick={handleDeleteGame} style={{margin:"0.25rem"}} value={data.id}>Delete</Button>
                }
                
            </div>
        )},
    ];

    return(
        <MovieGameProvider>
        <h1 style={{textAlign: "center", fontSize: "2.5rem"}}>Game List</h1>
        <div style={{ textAlign: "center", marginBottom: "20px" }}>
            {/* <Link to="/mobile-form"><Button>Add New Mobile App</Button></Link> */}
        </div>
        <Table columns={columns} dataSource={dataGame} id="appTable" rowKey={record => record.id} pagination={{ position: ["bottomCenter"] }} />
        </MovieGameProvider>
    );
}

export default GameTable;