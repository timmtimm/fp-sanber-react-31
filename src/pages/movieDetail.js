import React, { useContext, useEffect, useState } from "react";
import { useHistory, useParams } from "react-router-dom";
import { MovieGameContext } from "../context/MovieGameContext";

const MovieDetail = () =>
{
    let history = useHistory();

    const { detailMovie, setDataMovie, inputMovie, getMovieDetailByID } = useContext(MovieGameContext) ;

    const [ fetched, setFetched ] = useState(false);

    const { Id } = useParams();

    useEffect(() => {
        if(fetched == false)
        {
            if(Id !== undefined)
            {
                // console.log("masukx");
                // console.log(Id)
                getMovieDetailByID(Id);
                // console.log(inputMovie)
                // console.log(dataMovie)
            }
            setFetched(true);
        }
        // console.log(detailMovie);
        // console.log(Id);
        // form.setFieldsValue({...inputGame});
    }, [getMovieDetailByID]);

    return(
        <div>
            <img src={detailMovie.image_url} width={500} style={{display: "block", margin: "auto"}}/>
            <h1 style={{fontSize: "2rem", textAlign: "center"}}>{detailMovie.title} ({detailMovie.year})</h1>
            <hr />
            {/* <p><strong style={{fontSize: "1.25rem"}}>Release Year</strong><br /><span style={{fontSize: "1rem"}}>{detailMovie.year}</span></p> */}
            <p><strong style={{fontSize: "1.25rem"}}>Duration</strong><br /><span style={{fontSize: "1rem"}}>{detailMovie.duration} Minute</span></p>
            <p><strong style={{fontSize: "1.25rem"}}>Genre</strong><br /><span style={{fontSize: "1rem"}}>{detailMovie.genre}</span></p>
            <p><strong style={{fontSize: "1.25rem"}}>Description</strong><br /><span style={{fontSize: "1rem"}}>{detailMovie.description}</span></p>
            <p><strong style={{fontSize: "1.25rem"}}>Review</strong><br /><span style={{fontSize: "1rem"}}>{detailMovie.review}</span></p>
        </div>
    );
}

export default MovieDetail;