import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom";

import Home from "../pages/home";
import MovieTable from "../pages/movieTable";
import GameTable from "../pages/GameTable";

import GameForm from "../pages/GameForm";
import MovieForm from "../pages/MovieForm";
import LoginForm from "../pages/LoginForm"
import RegisterForm from "../pages/RegisterForm"
import MovieDetail from "../pages/movieDetail";
import GameDetail from "../pages/gameDetail";
import ChangePasswordForm from "../pages/changePasswordForm";

import Layout from "../component/Layout";

import { MovieGameProvider } from "../context/MovieGameContext"

export default function App() {
  return (
    <Router>
      	<MovieGameProvider>
			<Switch>
				<Route path="/" exact>
					<Layout content={<Home />}/>
				</Route>
				<Route path="/movie/table" exact>
					<Layout content={<MovieTable />}/>
				</Route>
				<Route path="/movie/detail/:Id">
					<Layout content={<MovieDetail />}/>
				</Route>
				<Route path="/movie/form" exact>
					<Layout content={<MovieForm />} />
				</Route>
				<Route path="/movie/form/edit/:Id" exact>
					<Layout content={<MovieForm />}/>
				</Route>

				<Route path="/game/table" exact>
					<Layout content={<GameTable />}/>
				</Route>
				<Route path="/game/detail/:Id" exact>
					<Layout content={<GameDetail />}/>
				</Route>
				<Route path="/game/form" exact>
					<Layout content={<GameForm />} />
				</Route>
				<Route path="/game/form/edit/:Id" exact>
					<Layout content={<GameForm />}/>
				</Route>
				
				<Route path="/login" exact>
					<Layout content={<LoginForm />}/>
				</Route>
				<Route path="/register" exact>
					<Layout content={<RegisterForm />}/>
				</Route>
				<Route path="/change-password" exact>
					<Layout content={<ChangePasswordForm />}/>
				</Route>
          	</Switch>
        </MovieGameProvider>
    </Router>
  );
}
